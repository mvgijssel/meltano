---
sidebarDepth: 2
---


# Marketing Ops

Please see a full list of all our [tooling here](https://meltano.com/handbook/tech-stack/).

## Website/blog

The website and blog are currently hosted on SiteGround. You can find the credentials in 1Password.

### WordPress

WordPress has automatic updates enabled so that we always run the latest stable release.

#### Plugins

Plugins are also automatically updated and typically update overnight. A ticket is automatically created in ZenDesk whenever plugins are updated.

### Troubleshooting

#### Website redirects to defaultwebpage.cgi

It's not clear why this happens, but clearing the cache resolves it. Follow SiteGround's instructions to [clear the cache](https://www.siteground.com/kb/clear-site-cache/). See [this issue](https://gitlab.com/meltano/meltano/-/issues/2886) for more information.

## Intercom

## Social

## Design

## Newsletter

## Community Management Tools
